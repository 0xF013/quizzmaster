const Joi = require('joi')
const createGenericValidator = require('./generic-validator')


const schema = Joi.object({
  name: Joi.string().required()
}).required()

module.exports = () => createGenericValidator({schema})