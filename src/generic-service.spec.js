const createGenericService = require('./generic-service')

describe('crud service', () => {
  const validator = {
    validate: jest.fn()
  }
  const mockResult = { some: 'result' }
  const repository = {
    insert: jest.fn(entity => mockResult)
  }
  const service = createGenericService({ repository, validator })

  describe('create', () => {
    it('validates, writes an object with a generated ID and returns a record', async () => {
      const payload = { some: 'Payload' }
      const record = await service.create(payload)
      expect(validator.validate).toHaveBeenCalledWith(payload) // we assume validator throws
      expect(repository.insert).toHaveBeenCalledWith({
        ...payload,
        id: expect.any(String)
      })
      expect(record).toBe(mockResult)
    })
  })
})
