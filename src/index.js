const loki = require('lokijs')

const createGenericService = require('./generic-service')

const createTeachingClassValidator = require('./teaching-class-validator')
const createAssignQuizzValidator = require('./assign-quizz-validator')
const createPersonValidator = require('./person-validator')
const createQuizzValidator = require('./quizz-validator')

const createAssignQuizz = require('./actions/assign-quizz')
const createSubmitQuizzAnswers = require('./actions/submit-quizz-answers')
const createGradeQuizzAssignment = require('./actions/grade-quizz-assignment')
const createCalculateSemesterGrades = require('./actions/calculate-semester-grades')

const storage = new loki('db', {
  adapter: new loki.LokiMemoryAdapter()
})

const teacherRepository = storage.addCollection('teachers')
const studentRepository = storage.addCollection('students')
const quizzRepository = storage.addCollection('quizzes')
const teachingClassRepository = storage.addCollection('teachingClasses')
const quizzAssigmentRepository = storage.addCollection('quizzAssignments')

const teacherService = createGenericService({
  repository: teacherRepository,
  validator: createPersonValidator()
})

const studentService = createGenericService({
  repository: studentRepository,
  validator: createPersonValidator()
})

const quizzService = createGenericService({
  repository: quizzRepository,
  validator: createQuizzValidator({ teacherRepository })
})

const teachingClassService = createGenericService({
  repository: teachingClassRepository,
  validator: createTeachingClassValidator({
    teacherRepository,
    teachingClassRepository,
    studentRepository
  })
})

const assignQuizzValidator = createAssignQuizzValidator({
  teacherRepository,
  studentRepository,
  quizzRepository,
  teachingClassRepository,
  quizzAssigmentRepository
})

const assignQuizz = createAssignQuizz({
  validator: assignQuizzValidator,
  repository: quizzAssigmentRepository
})

const submitQuizzAnswers = createSubmitQuizzAnswers({
  quizzAssigmentRepository,
  quizzRepository
})

const gradeQuizzAssignment = createGradeQuizzAssignment({
  quizzAssigmentRepository
})

const calculateSemesterGrades = createCalculateSemesterGrades({
  quizzAssigmentRepository
})

module.exports = {
  storage,
  teacherService,
  studentService,
  quizzService,
  teachingClassService,
  assignQuizz,
  submitQuizzAnswers,
  gradeQuizzAssignment,
  calculateSemesterGrades
}
