const Joi = require('joi')
const createGenericValidator = require('./generic-validator')
const validateExistence = require('./lib/validate-existence')

const schema = Joi.object()
  .keys({
    name: Joi.string().required(),
    teacherId: Joi.string().uuid().required(),
    questions: Joi.array().min(1).max(50).items(
      Joi.object()
        .keys({
          content: Joi.string().required(),
          options: Joi.array()
            .min(2)
            .max(10)
            .unique()
            .items(Joi.string().required())
            .required()
        })
        .required()
    )
  })
  .required()

module.exports = ({ teacherRepository }) =>
  createGenericValidator({
    schema,
    additionalCheck: ({ teacherId }) =>
      validateExistence({ id: teacherId, repository: teacherRepository })
  })
