const Joi = require('joi')
const createGenericValidator = require('./generic-validator')

describe('generic validator', () => {
  it('fails for data not matching schema', async () => {
    const schema = Joi.number().required()
    const validator = createGenericValidator({ schema })
    expect.assertions(1)
    try {
      await validator.validate('not a number')
    } catch (e) {
      expect(e).toMatchSnapshot()
    }
  })

  it('fails for failing aditional check', async () => {
    const schema = Joi.number().required()
    const validator = createGenericValidator({
      schema,
      additionalCheck: payload => {
        throw new Error(`Check not passed for value ${payload}`)
      }
    })
    expect.assertions(1)
    try {
      await validator.validate(14)
    } catch (e) {
      expect(e).toMatchSnapshot()
    }
  })

  it('passes if all is ok', async () => {
    const schema = Joi.number().required()
    const validator = createGenericValidator({
      schema,
      additionalCheck: payload => Promise.resolve('all is well')
    })
    const result = await validator.validate(14)
    expect(result).toBe('all is well')
  })
})
