const Joi = require('joi')
const createGenericValidator = require('./generic-validator')
const ValidationError = require('./lib/validation-error')

const schema = Joi.object()
  .keys({
    name: Joi.string().required(),
    teacherId: Joi.string().uuid().required(),
    students: Joi.array().items(Joi.string().uuid().required()).required()
  })
  .required()

module.exports = ({ studentRepository, teacherRepository }) =>
  createGenericValidator({
    schema,
    additionalCheck ({ teacherId, students }) {
      // TODO: refactor to validate existence
      if (!teacherRepository.findOne({ id: teacherId })) {
        throw new ValidationError(`Teacher with id: ${teacherId} not found`)
      }

      // This is very inefficient. Should be optimized in a real case scenario
      const validStudents = studentRepository.where(student =>
        students.includes(student.id)
      )
      if (validStudents.length !== students.length) {
        // The message better contain the list of invalid ids
        throw new ValidationError(`Some student ids are invalid`)
      }
    }
  })
