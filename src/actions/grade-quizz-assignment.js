const Joi = require('joi')
const validateExistence = require('../lib/validate-existence')

const gradeSchema = Joi.number().integer().min(0).max(4).required()

module.exports = ({quizzAssigmentRepository}) => {

  return async ({quizzAssigmentId, grade}) => {
    const quizzAssignment = quizzAssigmentRepository.findOne({id: quizzAssigmentId})
    validateExistence({
      id: quizzAssigmentId,
      repository: quizzAssigmentRepository
    })
    Joi.assert(grade, gradeSchema)

    quizzAssignment.grade = grade
    return quizzAssigmentRepository.update(quizzAssignment)
  }
}