const alasql = require('alasql')

// TODO: test this
alasql.fn.semester = dateString => {
  const date = new Date(dateString)
  const year = date.getFullYear()
  const month = date.getMonth()
  const dateOfMonth = date.getDate()
  if (month >= 0) {
    if (dateOfMonth >= 15) {
      return `${year}/II`
    } else {
      return `${year}/I`
    }

    return `${year}/I`
  }
}

module.exports = ({quizzAssigmentRepository}) => {
  return async () => {
    const assignments = quizzAssigmentRepository.find()

    // I am too dumb to do it without SQL
    return  alasql(`
      SELECT teacherId, studentId, SUM(grade) as totalGrade, semester(createdAt) as semester
      FROM ?
      GROUP BY teacherId, studentId, semester(createdAt)
    `, [assignments])
  }
}