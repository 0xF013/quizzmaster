const ValidationError = require('../lib/validation-error')
const validateExistence = require('../lib/validate-existence')


module.exports = ({quizzAssigmentRepository, quizzRepository}) => {

  return async ({quizzAssigmentId, answers}) => {
    const quizzAssignment = quizzAssigmentRepository.findOne({id: quizzAssigmentId})

    validateExistence({
      id: quizzAssigmentId,
      repository: quizzAssigmentRepository
    })

    const { questions } = quizzRepository.findOne({id: quizzAssignment.quizzId})
    // ok this format is really dumb but I caught a cold and my IQ took a hit
    // `answers` is an array where the index is the index in the questions array and the value is the index in the question's options array
    answers.forEach((optionIndex, questionIndex) => {
      const question = questions[questionIndex]
      if (!question) {
        throw new ValidationError(`Question with index ${questionIndex} not present in quizz ${quizzAssignment.quizzId}`)
      }
      const option = question.options[optionIndex]
      if (!option) {
        throw new ValidationError(`Options with index ${optionIndex} for question with index ${questionIndex} not present in quizz ${quizzAssignment.quizzId}`)
      }

      quizzAssignment.answers[questionIndex] = optionIndex
    })

    return quizzAssigmentRepository.update(quizzAssignment)
  }
}