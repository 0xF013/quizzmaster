const uuid = require('uuid/v4')

module.exports = ({validator, repository}) => {
  return async (payload) => {
    await validator.validate(payload)
    return repository.insert({
      ...payload,
      answers: [],
      id: uuid(),
      createdAt: new Date()
    })
  }
}