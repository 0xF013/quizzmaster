const Joi = require('joi')
const createGenericValidator = require('./generic-validator')
const ValidationError = require('./lib/validation-error')
const validateExistence = require('./lib/validate-existence')

const schema = Joi.object()
  .keys({
    teacherId: Joi.string().uuid().required(),
    studentId: Joi.string().uuid().required(),
    quizzId: Joi.string().uuid().required()
  })
  .required()

module.exports = ({
  teacherRepository,
  studentRepository,
  quizzRepository,
  teachingClassRepository,
  quizzAssigmentRepository
}) => {
  validateQuizzBelongsToTeacher = ({ teacherId, quizzId }) => {
    if (!quizzRepository.findOne({ id: quizzId, teacherId })) {
      throw new ValidationError(
        `Quizz '${quizzId}' does not belong to teacher ${teacherId}`
      )
    }
  }

  const validateStudentBelongsToTeacherClass = ({ studentId, teacherId }) => {
    if (
      !teachingClassRepository
        .findOne({ teacherId })
        .students.includes(studentId)
    ) {
      throw new ValidationError(
        `Student '${studentId}' is not in a class of teacher ${teacherId}`
      )
    }
  }

  const validateDoubleAssigment = queryParams => {
    if (quizzAssigmentRepository.findOne(queryParams)) {
      throw new Error(`Double assignment for ${JSON.stringify(queryParams)}`)
    }
  }

  return createGenericValidator({
    schema,
    additionalCheck: async ({ teacherId, studentId, quizzId }) => {
      validateExistence({
        repository: teacherRepository,
        id: teacherId
      })

      validateExistence({
        repository: studentRepository,
        id: studentId
      })

      validateExistence({
        repository: quizzRepository,
        id: quizzId
      })

      validateQuizzBelongsToTeacher({ teacherId, quizzId })
      validateStudentBelongsToTeacherClass({ teacherId, studentId })
      validateDoubleAssigment({ teacherId, studentId, quizzId })
    }
  })
}
