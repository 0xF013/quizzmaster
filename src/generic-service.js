const uuid = require('uuid/v4')

module.exports = ({ repository, validator }) => {
  return {
    async create (payload) {
      await validator.validate(payload)
      return repository.insert({
        ...payload,
        id: uuid()
      })
    }
  }
}
