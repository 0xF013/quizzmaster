const ValidationError  = require('./validation-error')

module.exports = ({id, repository}) => {
  if (!repository.findOne({id})) {
    throw new ValidationError(`Entity with id '${id}' not found in ${repository.name}`)
  }
}