const Joi = require('joi')

module.exports = ({schema, additionalCheck}) => {
  return {
    async validate(payload) {
      Joi.assert(payload, schema)
      return additionalCheck && await additionalCheck(payload)
    }
  }
}