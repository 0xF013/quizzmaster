const {
  storage,
  teacherService,
  studentService,
  quizzService,
  teachingClassService,
  assignQuizz,
  submitQuizzAnswers,
  gradeQuizzAssignment,
  calculateSemesterGrades
} = require('../src')

const logStorageState = () =>
  new Promise(resolve => {
    storage.save(() => {
      resolve(
        console.log(
          JSON.stringify(
            JSON.parse(storage.toJson()).collections.map(c => ({
              name: c.name,
              data: c.data
            })),
            null,
            2
          )
        )
      )
    })
  })

const createWindQuizz = (teacherId) =>
  quizzService.create({
    name: 'Wind Quizz One',
    teacherId: teacherId,
    questions: [
      {
        content: 'What of these is a wind?',
        options: ['Zipper', 'Zephyr', 'Zephir', 'Sapphire']
      },

      {
        content: 'What of these is a wind?',
        options: ['Messing', 'Messi', 'Mongoose', 'Monsoon']
      }
    ]
  })

  const createAnotherWindQuizz = (teacherId) =>
  quizzService.create({
    name: 'Water quizz',
    teacherId: teacherId,
    questions: [
      {
        content: 'Question one',
        options: ['a', 'b', 'c', 'd']
      },

      {
        content: 'Question two',
        options: ['a', 'b', 'c', 'd']
      }
    ]
  })

describe('General flow', () => {
  it('Takes the happy path', async () => {
    const teacher = await teacherService.create({ name: 'Ivan Drago' })
    const student = await studentService.create({ name: 'Johnny Bravo' })
    const secondStudent = await studentService.create({ name: 'Ali G' })
    const quizz = await createWindQuizz(teacher.id)
    const secondQuizz = await createAnotherWindQuizz(teacher.id)
    await teachingClassService.create({
      name: 'Aerothurgy 101',
      teacherId: teacher.id,
      students: [student.id, secondStudent.id]
    })

    const quizzAssignment = await assignQuizz({
      teacherId: teacher.id,
      studentId: student.id,
      quizzId: quizz.id
    })

    const secondQuizzAssignment = await assignQuizz({
      teacherId: teacher.id,
      studentId: student.id,
      quizzId: secondQuizz.id
    })

    const thirdQuizzAssignment = await assignQuizz({
      teacherId: teacher.id,
      studentId: secondStudent.id,
      quizzId: secondQuizz.id
    })

    // index is the question index in the questions array
    // value is the index in the options array
    const answers = []
    answers[1] = 2

    await submitQuizzAnswers({
      quizzAssigmentId: quizzAssignment.id,
      answers
    })

    const secondRoundOfAnswers = []
    secondRoundOfAnswers[0] = 3

    await submitQuizzAnswers({
      quizzAssigmentId: quizzAssignment.id,
      answers: secondRoundOfAnswers
    })

    await gradeQuizzAssignment({
      quizzAssigmentId: quizzAssignment.id,
      grade: 3 // TODO: use contants
    })

    await gradeQuizzAssignment({
      quizzAssigmentId: secondQuizzAssignment.id,
      grade: 2 // TODO: use contants
    })

    await gradeQuizzAssignment({
      quizzAssigmentId: thirdQuizzAssignment.id,
      grade: 1
    })

    const grades = await calculateSemesterGrades()
    console.log(grades)

    // TODO: mock dates in order to have idempotent tests
    expect(grades[0].teacherId).toBe(teacher.id)
    expect(grades[0].studentId).toBe(student.id)
    expect(grades[0].totalGrade).toBe(5)
    expect(grades[0].semester).toBe('2018/II')




    // await logStorageState()
  })
})
