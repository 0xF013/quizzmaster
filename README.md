# Quizmaster

## Requirements

Please use your most proficient programming language to create object oriented design and
use test driven development to implement classes and methods with appropriate data structure
and test the code for the following scenario.

Please add comments describing any assumptions
you make:

- There are Teachers
- There are Students
- Students are in classes that teachers teach
- Teachers can create multiple quizzes with many questions (each question is multiple choice)
- Teachers can assign quizzes to students
- Students solve/answer questions to complete the quiz, but they don't have to complete it at once. (Partial submissions can be made.
- Quizzes need to get graded
- For each teacher, they can calculate each student's total grade accumulated over a semester for their classes

## Assumptions

- Instances need to only be _created_ in this example, other CRUD operations are left to the reader as an imagination exercise.
- Entities have only one or two basic props (i.e. `name`) that I picked as the most descriptive.
- For the sake of total grade calculation simplicity, grades are assumed to be the following:
  - A = 4
  - B = 3
  - C = 2
  - D = 1
  - F = 0
- Quizzes can only be created only once together with questions and options for them.
- Quizzes have a maximum of 50 questions just in case.
- Quizzes options are limited from 2 to 10.
- Quizz assignment ACL is performed on a higher level i.e. quizz submission trusts the assigment id to be correct.
- Quizzes can only be assigned to students in a teacher's class.
- Same as above goes for grading.
- We do not track answers submission events, just update the answers.
- An answer can be resubmitted.
- A quizz assignment can be left not graded, it is not an automatic F and is not included in final calculations.
- An incomplete quizz can still be graded.
- Semesters are:
  - I: before 1/15
  - II: after 1/15
- Total grade is calculated as the summ of the grades for each graded quizz during a semester.

## Notes before reading code

- I didn't do it using pure TDD. My flow included a test for the happy path that would fail until everything worked. I didn't have enough time to cover negative cases in tests, but I made sure to see them fail while writing the main flow spec

- I am using lokijs in order to not write my own generic repository, since it provided a in-memory array for each collection
- I am using joi to validate input params and partially to cover up for what TS should have done.
- There is a lot of async/await going on around sync code. This is to allow for a future async flow if it is needed.
- There are tests for generic factories, but the specific services tests are missing due to time constraints.
- One might find my data format weird, this is due to me usually using SQL over knex.
- No typing system (didn't do much TS on the backend lately). For this reason, most methods destructure the incoming args in order to filter out possible unwaranted props.
- I am using DTO's and async services injected as dependencies in order to be able to swap the implementation to a microservice approach at some point.
  - Repositories can become any local, remote or cloud storage
  - DTO's are not class instances for the reason that they might just be plain JSON passed between possible future microservices via transport layers like Amazon SQS/SNS or RabbitMQ.
  - For the reason mentioned above and lack of TS, I am using simple factories instead of classes.
- Validators throw a validation error that can be caught by the caller or go way up to the express/koa error handler and display a specific message/coded based on the type (i.e. `404` vs `500`) or log different error levels to Rollbar / Sentry.
- Some code might look identical in implementation at a first glance, but the common functionality is not abstracted away for one of the following reasons:
  - It is identical because the example is simple. With more methods (i.e. additional CRUD operations or validations) the implementation will be quite different across similar entities.
  - It is identical because the implementation happens to be the same only by coincidence (i.e. a creation validation might look similar to a prerequisite validation, but belongs to a different domain).
  - As a personal preference, I tend to extract duplication when it happens the third time, as extracting duplication from two instances has the same complexity as simply having two exact implementations, if not highter).

## Installation and usage

- Using node.js v10
- `npm i`
- `npm test`